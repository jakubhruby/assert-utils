# assert-utils
Assertion tool that deeply compares value to its definition.

## Install
```bash
$ yarn add assert-utils
```

## Usage
```javascript
const
	assertUtils = require('assert-utils'),
	{assert} = assertUtils;

function checkIsString(myText) {
	assert.type(myText, 'string');
}

checkIsString('this is string'); // valid, outputs nothing
checkIsString(2); // invalid, throws an error with message 'Assertion error: expected "string" but got "number"'
```

## API

### assert.value(value, expectation, strict)
Checks whether the `value` equals to the `expectation`. Throws error if not.

**value** | `any`  

**expectation** | `any`  

**strict** | `boolean` _optional_  
Default is `false`.

**strict** | `boolean` _optional_
Default is `false`.


```javascript
assert.value('1' + 0, '10'); // valid
assert.value('1' + 0, 10); // valid
assert.value('1' + 0, 10, true); // invalid
assert.value('1' + 0, 1); // invalid
```

### assert.type(value, definition)
Deeply checks whether the whole `value` equals to its `definition`. If the `definition` is an object, it recursively checks all its properties.

**value** | `any`  

**definition** | `any`  
`definition` basically accepts the same value as returned by built in `typeof` operator. In addition, there are some shortcuts:
  - `'[]'` for `array`
  - `'{}'` or `{}` for `object`

#### Example
```javascript
let
	value = {
		number: 200,
		string: 'this is string',
		function: function() {},
		object: {
			stringProperty: 'also string'
		}
	};

assert.type(value, {
	number: 'number',
	string: 'string',
	function: 'function',
	object: {
		stringProperty: 'string'
	}
});
```

#### Optional properties
If you want to mark any definition _optional_, just add a `?` before the first letter.

```javascript
assert.type('this is string', '?string'); // valid
assert.type(undefined, '?string'); // also valid
assert.type(undefined, 'string'); // invalid
```

In case of optional object definition you can use special property `_required: true`. Default is `true`.

```javascript
// valid
assert.type({
	string: 'this is string'
}, {
	string: 'string'
})

// also valid
assert.type(undefined, {
	_required: false,
	string: 'string'
})

// invalid
assert.type(undefined, {
	string: 'string'
})
```

#### Multiple possible types
In case you expect multiple type, just divide them by `|`.

```javascript
// valid
assert.type({
	stringOrNumber: 2
}, {
	stringOrNumber: 'string|number'
})

// also valid
assert.type({
	stringOrNumber: 'this is string'
}, {
	stringOrNumber: '?string|number'
})

// invalid - "?" has to be first
assert.type({
	stringOrNumber: 'this is string'
}, {
	stringOrNumber: 'string|?number'
})
```

#### Equal properties
By default only the properties included in the `definition` object are being checked. If the tested object has some additional properties, it just passes them as valid. If you want to check there are no additional properties beside those in the `definition`, use a special property `_strict: true`. Default is `false`:

```javascript
// valid
assert.type({
	string: 'this is string'
}, {
	string: 'string'
})

// also valid
assert.type({
	string: 'this is string',
	number: 2
}, {
	string: 'string'
})

// invalid
assert.type({
	string: 'this is string',
	number: 2
}, {
	_strict: true,
	string: 'string'
})
```

#### Specific value instead of type
In case you want to mix up type and value checks, use a special property `_value`. Of cource you can use it along with `_strict` property:
```javascript
// valid
assert.type(1, {
	_value: 1
})

// valid
assert.type(1, {
	_value: '1'
})

// invalid
assert.type(1, {
	_value: 'number'
})

// invalid
assert.type(1, {
	_value: '1',
	_strict: true
})
```

## Testing
Tests are using [AVA library](https://github.com/avajs/ava)

```bash
$ yarn test                              // run all tests
$ yarn test -- -m 'Test title RegExp'    // run test with matching title
```
