class Assert {
	/**
	 * Deeply compares value to an expected definition
	 * @param  {*} value
	 * @param  {*} def
	 * @return {boolean}
	 */
	type(value, def) {
		let
			error = this._checkItem(value, def);

		if (error) {
			this._handleError(error);
		}
		else {
			return true;
		}
	}

	/**
	 * Compares value to an expectation
	 * @param  {*} value
	 * @param  {*} expectedValue
	 * @param  {boolean} [strict=false]
	 * @return {boolean}
	 */
	value(value, expectedValue, strict) {
		let
			error = this._checkValue(value, expectedValue, strict);

		if (error) {
			this._handleError(error);
		}
		else {
			return true;
		}
	}

	/**
	 * Creates error message
	 * @param {Object} err
	 * @param {Object} [err.name] property name
	 * @param {Object} [err.expected] expected type or value
	 * @param {Object} [err.got] what we got
	 * @throws {TypeError}
	 */
	_handleError(err) {
		let
			message;

		if (err.expected && err.got) {
			if (err.name) {
				message = `expected "${err.name}" to be "${err.expected}" but got ${err.got}`;
			}
			else {
				message = `expected "${err.expected}" but got ${err.got}`;
			}
		}
		else if (err.name) {
			message = `unexpected property ${err.name}`;
		}

		throw new TypeError('Assertion error: ' + message);
	}

	/**
	 * Compares value to an expectation
	 * @param  {*} value
	 * @param  {*} expectedValue
	 * @param  {boolean} [strict=false]
	 * @return {boolean}
	 */
	_checkValue(value, expectedValue, strict) {
		if (
			(strict && value !== expectedValue)
			|| (value != expectedValue)
		) {
			return {
				expected: expectedValue,
				got: value
			};
		}
	}

	/**
	 * Recursively compares value to an expected definition
	 * @param  {*} value
	 * @param  {*} def
	 * @param  {string} [name]
	 * @return {boolean}
	 */
	_checkItem(value, def, name) {
		let
			item, valid, strict,
			required = true;

		if (typeof def === 'string') {
			if (def.substr(0, 1) === '?') {
				required = false;
				def = def.substr(1);
			}

			valid = def.split('|').some(subdef => {
				if (subdef === '{}') {
					return typeof value === 'object';
				}
				else if (subdef === 'null') {
					return value === null;
				}
				else if (subdef === '[]' || subdef === 'array') {
					return value instanceof Array;
				}
				else if (subdef === 'element') {
					return value instanceof HTMLElement;
				}
				else {
					return typeof value === subdef;
				}
			});

			if (!required) {
				valid = valid || value === undefined;
			}

			if (!valid) {
				return {
					name: name,
					expected: def,
					got: typeof value
				};
			}
		}
		else if (typeof def === 'object') {
			if (def.hasOwnProperty('_required')) {
				required = def._required;
				delete def._required;
			}

			if (def.hasOwnProperty('_strict')) {
				strict = def._strict;
				delete def._strict;
			}

			if (def.hasOwnProperty('_value')) {
				try {
					return this._checkValue(value, def._value, strict);
				}
				catch (err) {
					err.name = name;
					this._handleError(err);
				}
			}

			if (value === undefined) {
				if (required) {
					return {
						name: name,
						expected: 'object',
						got: 'undefined'
					};
				}
			}
			else if (typeof value !== 'object') {
				return {
					name: name,
					expected: 'object',
					got: typeof value
				};
			}
			else {
				for (item in def) {
					let
						error,
						fullName = name ? name + '.' + item : item;

					error = this._checkItem(value[item], def[item], fullName);

					if (error) {
						return error;
					}
				}

				if (strict) {
					let
						keys = Object.keys(value);

					for (let i = 0; i < keys.length; i++) {
						if (!def.hasOwnProperty(keys[i])) {
							return {
								name: keys[i]
							};
						}
					}
				}
			}
		}
	}
}
exports.Assert = Assert;
