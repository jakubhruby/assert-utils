const
	{Assert} = require('./src/Assert.js');

exports = {
	Assert: Assert,
	assert: new Assert()
};

// CommonJS
module.exports = exports;
